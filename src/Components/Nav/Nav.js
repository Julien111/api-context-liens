import React from "react";
import "./Nav.css";
import {NavLink} from "react-router-dom";

export default function Nav() {
  return (
    <nav>
      <ul className="navbar">
        <NavLink exact activeClassName="focus" to="/">
          <li className="item">Accueil</li>
        </NavLink>
        <NavLink exact activeClassName="focus" to="/contact">
          <li className="item">Contact</li>
        </NavLink>
        <NavLink exact activeClassName="focus" to="/projet">
          <li className="item">Projet</li>
        </NavLink>
      </ul>
    </nav>
  );
}
