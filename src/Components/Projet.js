import React from 'react'
import { useHistory, useParams } from 'react-router'

export default function Projet() {

    const history = useHistory();
    const {slug} = useParams();

    console.log(slug);

    return (
        <>
            <h1 className="projet-title">Projet</h1>

            <p>{slug}</p>

            <button onClick={() => history.push("/")}>
                Go to home
            </button>
        </>
    )
}
