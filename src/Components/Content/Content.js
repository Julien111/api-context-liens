import React, {useContext} from 'react';
import BtnToggle from "../BtnToggle/BtnToggle";
import "./Content.css";
import {ThemeContext} from "../../Components/Context/ThemeContext";

export default function Content() {

       const { theme } = useContext(ThemeContext);

       console.log(theme);

    return (    


        <div className="container">
            <BtnToggle />
            <h1>Lorem ipsum dolor</h1>

            <p className="content-txt">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Numquam esse quidem id necessitatibus eligendi! Ipsa, ad. Facere fugit impedit tempore, iste, quidem molestias numquam accusantium atque quia debitis voluptates aliquam.
            Minus dolor officiis totam illo quae magnam ipsum assumenda corporis beatae. Aperiam ratione quae facilis perspiciatis optio quidem architecto iste, velit consequatur incidunt, molestias tempora, fuga reprehenderit alias vitae quisquam.
            Pariatur natus temporibus animi eaque tempora tempore esse rem, odio neque blanditiis eum iure deserunt ad, quam et laborum voluptatibus eos sit sequi nobis iste. Odit non maiores optio tenetur!
            Deleniti tempora, laudantium ipsa numquam odit tempore harum temporibus commodi, nihil beatae vero, culpa facilis fugit voluptate saepe. Omnis eius ea ipsam illo mollitia voluptates aspernatur, ab magni nam necessitatibus.
            Soluta placeat dolores non, dignissimos exercitationem sunt, error sapiente autem iure odit ut voluptate culpa tempore consectetur beatae excepturi dolore amet nobis corrupti maxime cum iusto nemo nihil? Suscipit, rerum!</p>
        </div>
    )
}
