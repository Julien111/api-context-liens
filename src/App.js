import './App.css';
import Content from "./Components/Content/Content";
import ThemeContextProvider from "./Components/Context/ThemeContext";
// import Accueil from "./Components/Accueil";
// import Contact from "./Components/Contact";
// import Projet from "./Components/Projet";
// import Nav from "./Components/Nav/Nav";
// import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

function App() {
  return (
    <>
      <ThemeContextProvider>
      <Content />
      </ThemeContextProvider>
      {/* <Router>
        <Nav />
        <Switch>
          <Route path="/" exact component={Accueil} />
          <Route path="/projet/:slug" exact component={Projet} />
          <Route path="/Contact" exact component={Contact} />
          <Route path="/" component={() => <div>Erreur 404</div>} />
        </Switch>
      </Router> */}
    </>
  );
}

export default App;
